#![allow(dead_code)]
#![no_std]

use embedded_hal::blocking::i2c;

use ufmt::derive::uDebug;
use ufmt::uDebug;
use ufmt::uWrite;
use ufmt::Formatter;

use core::marker::PhantomData;

pub trait Variation {
   fn address() -> u8;

   fn scale_diff_pressure_pascal_factor() -> u16;
   fn scale_diff_pressure_h2o_factor() -> u16;
   fn scale_temperature_factor() -> u16 {
      200
   }
}

macro_rules! create_variation {
   ($n:ident, $addr:literal, $pp:literal, $ph:literal) => {
      pub struct $n();

      impl Variation for $n {
         fn address() -> u8 {
            $addr
         }

         fn scale_diff_pressure_pascal_factor() -> u16 {
            $pp
         }
         fn scale_diff_pressure_h2o_factor() -> u16 {
            $ph
         }
      }
   };
}

create_variation!(SDP800_500PA, 0x25, 60, 14945);
create_variation!(SDP810_500PA, 0x25, 60, 14945);
create_variation!(SDP800_125PA, 0x25, 240, 59780);
create_variation!(SDP810_125PA, 0x25, 240, 59780);
create_variation!(SDP801_500PA, 0x26, 60, 14945);
create_variation!(SDP811_500PA, 0x26, 60, 14945);

pub trait State {}

pub struct Idle;
pub struct Continuous;
//pub struct Sleep;

impl State for Idle {}
impl State for Continuous {}

#[derive(uDebug)]
pub struct SDP8xxIdentifier {
   pub product_no: u32,
   pub serial_no: [u32; 2],
}

pub struct SDP8xxPressure<T: Variation> {
   pub pressure: i16,
   pub temperature: Option<i16>,
   pub scale: Option<u16>,
   _v: PhantomData<T>,
}

pub struct SDP8xx<T: Variation, D, S: State>
where
   D: i2c::Read<Error = <D as i2c::Write>::Error> + i2c::Write<Error = <D as i2c::WriteRead>::Error> + i2c::WriteRead,
{
   _device: PhantomData<D>,
   _variation: PhantomData<T>,
   _state: PhantomData<S>,
}

pub enum TemperatureCompensation {
   MassFlow,
   DifferentialPressure,
}

pub enum GatherExtra {
   None,
   Temperature,
   TemperatureAndScale,
}

impl<T: Variation> uDebug for SDP8xxPressure<T> {
   fn fmt<W>(&self, f: &mut Formatter<W>) -> Result<(), W::Error>
   where
      W: uWrite + ?Sized,
   {
      f.debug_struct("SDP8xxPressure")?
         .field("pressure", &self.pressure)?
         .field("temperature", &self.temperature)?
         .field("scale", &self.scale)?
         .finish()
   }
}

//values can be converted by dividing by the scale factor. §6.5
impl<T: Variation> SDP8xxPressure<T> {
   /// Converts pressure level into human parsable values in Pascals (N/m^2)
   pub fn human_pressure(&self) -> f32 {
      (self.pressure as f32) / (T::scale_diff_pressure_pascal_factor() as f32)
   }

   /// Converts pressure level into human parsable values in H2O (Inch/H2O)
   pub fn human_pressure_h2o(&self) -> f32 {
      (self.pressure as f32) / (T::scale_diff_pressure_h2o_factor() as f32)
   }

   /// Converts temperature level into human parsable values Celsius (℃)
   pub fn human_temperature(&self) -> Option<f32> {
      self
         .temperature
         .map(|v| (v as f32) / (T::scale_temperature_factor() as f32))
   }
}

impl<T, D> SDP8xx<T, D, Idle>
where
   T: Variation,
   D: i2c::Read<Error = <D as i2c::Write>::Error> + i2c::Write<Error = <D as i2c::WriteRead>::Error> + i2c::WriteRead,
{
   /// Creates a new SDP8xx based on the types provided
   pub fn new() -> SDP8xx<T, D, Idle> {
      SDP8xx {
         _device: PhantomData,
         _variation: PhantomData,
         _state: PhantomData,
      }
   }

   /// Forces device into low power mode
   ///
   /// # Arguments
   /// * `device` - I2C device to read and write from
   pub fn sleep_device(&mut self, device: &mut D) -> Result<(), <D as i2c::Write>::Error> {
      device.write(T::address(), &[0x36, 0x77])
   }

   /// Reads internal ids, producing product and serial ids
   ///
   /// # Arguments
   /// * `device` - I2C device to read and write from
   pub fn read_product_id(&mut self, device: &mut D) -> Result<SDP8xxIdentifier, <D as i2c::WriteRead>::Error> {
      let mut buff: [u8; 18] = [0; 18];

      device.write(T::address(), &[0x36, 0x7C])?;
      device
         .write_read(T::address(), &[0xE1, 0x02], &mut buff)
         //todo check CRC
         .map(|_| SDP8xxIdentifier {
            product_no: u32::from_be_bytes([buff[0], buff[1], buff[3], buff[4]]),
            serial_no: [
               u32::from_be_bytes([buff[6], buff[7], buff[9], buff[10]]),
               u32::from_be_bytes([buff[12], buff[13], buff[15], buff[16]]),
            ],
         }) //incorrect
   }

   //45ms till responce
   /*pub fn mass_flow_avg_timed(&mut self, device: &mut D, gather:GatherExtra) -> Result<SDP8xxPressure<T>, <D as i2c::WriteRead>::Error> {
           split_gather!([0x36, 0x24], device, gather)
   }*/
   /*pub fn diff_pressure_avg_timed(&mut self, device: &mut D, gather:GatherExtra) -> Result<SDP8xxPressure<T>, <D as i2c::WriteRead>::Error> {
       split_gather!([0x36, 0x2F], device, gather)
   }*/

   /// Sensor Read, Clock Stretching version
   ///
   /// # Arguments
   /// * `device` - I2C device to read and write from
   /// * `tc` - Temperature compensation mode
   /// * `gather` - Data values to read. Default is just pressure
   pub fn read_clock_stretch(
      &mut self, device: &mut D, tc: TemperatureCompensation, gather: GatherExtra,
   ) -> Result<SDP8xxPressure<T>, <D as i2c::WriteRead>::Error> {
      let cmd = match tc {
         TemperatureCompensation::MassFlow => [0x37, 0x26],
         TemperatureCompensation::DifferentialPressure => [0x37, 0x2D],
      };

      match gather {
         GatherExtra::None => {
            let mut buff: [u8; 3] = [0; 3];
            //todo check CRC
            device
               .write_read(T::address(), &cmd, &mut buff)
               .map(|_| SDP8xxPressure {
                  pressure: i16::from_be_bytes([buff[0], buff[1]]),
                  temperature: None,
                  scale: None,
                  _v: PhantomData,
               })
         }
         GatherExtra::Temperature => {
            let mut buff: [u8; 6] = [0; 6];
            //todo check both CRC;
            device
               .write_read(T::address(), &cmd, &mut buff)
               .map(|_| SDP8xxPressure {
                  pressure: i16::from_be_bytes([buff[0], buff[1]]),
                  temperature: Some(i16::from_be_bytes([buff[3], buff[4]])),
                  scale: None,
                  _v: PhantomData,
               })
         }
         GatherExtra::TemperatureAndScale => {
            let mut buff: [u8; 9] = [0; 9];
            //todo check both CRC;
            device
               .write_read(T::address(), &cmd, &mut buff)
               .map(|_| SDP8xxPressure {
                  pressure: i16::from_be_bytes([buff[0], buff[1]]),
                  temperature: Some(i16::from_be_bytes([buff[3], buff[4]])),
                  scale: Some(u16::from_be_bytes([buff[6], buff[7]])),
                  _v: PhantomData,
               })
         }
      }
   }

   // 2ms reset time
   /*pub fn soft_reset(&mut self, device: &mut D) -> Result<(), <D as i2c::Write>::Error> {
       device.write(0x00, &[0x06])
   }*/

   /// Enters device into continuous read mode.
   ///
   /// Averages value based on §5.2
   /// # Arguments
   /// * `device` - I2C device to read and write from
   /// * `tc` - Temperature compensation mode
   pub fn start_continuous_mode_avg(
      self, device: &mut D, tc: TemperatureCompensation,
   ) -> Result<SDP8xx<T, D, Continuous>, (SDP8xx<T, D, Idle>, <D as i2c::Write>::Error)> {
      let cmd = match tc {
         TemperatureCompensation::MassFlow => [0x36, 0x03],
         TemperatureCompensation::DifferentialPressure => [0x36, 0x15],
      };

      device
         .write(T::address(), &cmd)
         .map(|_| SDP8xx {
            _device: PhantomData,
            _variation: PhantomData,
            _state: PhantomData,
         })
         .map_err(|e| (self, e))
   }

   /// Enters device into continuous read mode.
   ///
   /// New value available every 0.5ms
   /// # Arguments
   /// * `device` - I2C device to read and write from
   /// * `tc` - Temperature compensation mode
   pub fn start_continuous_mode_repeat(
      self, device: &mut D, tc: TemperatureCompensation,
   ) -> Result<SDP8xx<T, D, Continuous>, (SDP8xx<T, D, Idle>, <D as i2c::Write>::Error)> {
      let cmd = match tc {
         TemperatureCompensation::MassFlow => [0x36, 0x08],
         TemperatureCompensation::DifferentialPressure => [0x36, 0x1E],
      };

      device
         .write(T::address(), &cmd)
         .map(|_| SDP8xx {
            _device: PhantomData,
            _variation: PhantomData,
            _state: PhantomData,
         })
         .map_err(|e| (self, e))
   }
}

impl<T, D> SDP8xx<T, D, Continuous>
where
   T: Variation,
   D: i2c::Read<Error = <D as i2c::Write>::Error> + i2c::Write<Error = <D as i2c::WriteRead>::Error> + i2c::WriteRead,
{
   /// Sensor Read in Continuous mode
   ///
   /// # Arguments
   /// * `device` - I2C device to read and write from
   /// * `gather` - Data values to read. Default is just pressure
   fn read(&mut self, device: &mut D, gather: GatherExtra) -> Result<SDP8xxPressure<T>, <D as i2c::Read>::Error> {
      match gather {
         GatherExtra::None => {
            let mut buff: [u8; 3] = [0; 3];
            //todo check CRC
            device.read(T::address(), &mut buff).map(|_| SDP8xxPressure {
               pressure: i16::from_be_bytes([buff[0], buff[1]]),
               temperature: None,
               scale: None,
               _v: PhantomData,
            })
         }
         GatherExtra::Temperature => {
            let mut buff: [u8; 6] = [0; 6];
            //todo check both CRC;
            device.read(T::address(), &mut buff).map(|_| SDP8xxPressure {
               pressure: i16::from_be_bytes([buff[0], buff[1]]),
               temperature: Some(i16::from_be_bytes([buff[3], buff[4]])),
               scale: None,
               _v: PhantomData,
            })
         }
         GatherExtra::TemperatureAndScale => {
            let mut buff: [u8; 9] = [0; 9];
            //todo check both CRC;
            device.read(T::address(), &mut buff).map(|_| SDP8xxPressure {
               pressure: i16::from_be_bytes([buff[0], buff[1]]),
               temperature: Some(i16::from_be_bytes([buff[3], buff[4]])),
               scale: Some(u16::from_be_bytes([buff[6], buff[7]])),
               _v: PhantomData,
            })
         }
      }
   }

   /// Exits continuous mode and returns to the `Idle` mode
   ///
   /// # Arguments
   /// * `device` - I2C device to read and write from
   fn stop_continuous_mode(
      self, device: &mut D,
   ) -> Result<SDP8xx<T, D, Idle>, (SDP8xx<T, D, Continuous>, <D as i2c::Write>::Error)> {
      device
         .write(T::address(), &[0x3F, 0xF9])
         .map(|_| SDP8xx {
            _device: PhantomData,
            _variation: PhantomData,
            _state: PhantomData,
         })
         .map_err(|e| (self, e))
   }
}
